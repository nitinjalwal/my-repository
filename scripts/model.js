var DBZ_model=new function(){
    var sfss = null;
    var zoneName=null;
    var loginUserData=null;
    var roomData= null;
    
    this.setZone=function(args){ zoneName=args; };
    this.setLoginData=function(args){ loginUserData=args; };
    this.setRoomData=function(args){ roomData=args; };
    
    this.setSFSobj = function(param){
        sfss= param;
        sfsBindEvents.setSFSobj(sfss);
    };
    
    
    this.joinRoomRequest =function(){
        console.log("model joinRoomRequest");
        sfss.send(new SFS2X.Requests.System.JoinRoomRequest("classic"));
    };
    
    this.loginRequest=function(loginName){
        sfss.send(new SFS2X.Requests.System.LoginRequest(loginName, "", null, zoneName));
    };
    
    this.onRoomJoined=function(){
        trace("player list:");
        var pl=roomData.getPlayerList();
        trace(pl);
        
        $.each(pl,function(key,val){
            console.log(key+" -- "+val);
            $.each(val,function(k,value){console.log("#"+k+" --> "+value);});
        });
        
    };
    
};