(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] 
                                   || window[vendors[x]+'CancelRequestAnimationFrame'];
    }
 
    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
 
    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());

function startDBZ() {
    trace("start");
    var zoneName="DBZ";
    
    var sfss=new SFS2X.SmartFox({
        "host"  :"192.168.1.33",
        "port"  :8888,
        "debug" : false,
        "zone"  : zoneName
    });
    DBZ_model.setSFSobj(sfss);
    DBZ_model.setZone(zoneName);
    
    sfsBindEvents.bindEvents();
    DBZ_controller.bindEvents();
    
    sfss.connect();
    
    //$("#goku").addClass("gokuReady");
    //$("#vegeta").addClass("vegetoReady");
    
    //requestAnimationFrame(DBZ_controller.gameLoop);
}


function trace(text,mode) {
    switch (mode) {
        case 1:
            $("#trace").html(text);
            break;
        
        case 2:
            $("#trace").append("<br>"+text);
            break;
        
        case 3:
            alert(text);
            break;
        
        default:
            console.log(text);
            break;
    }
}