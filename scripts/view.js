var DBZ_view= new function(){
    
    var hasTouch = 'ontouchstart' in window;
    var startEvent = hasTouch ? 'touchstart' : 'mousedown';
    var moveEvent = hasTouch ? 'touchmove' : 'mousemove';
    var endEvent = hasTouch ? 'touchend' : 'mouseup';
    
    
    this.bindevents= function(){
        trace("bind view event");
        
        $("#login").on(endEvent,function(){
            if ($("#loginName").val()!=null && $("#loginName").val()!="") {
                DBZ_model.loginRequest($("#loginName").val());
            }
        });
        
        $("#choosePlayer div").on(endEvent,function(){
            
            var temp=this.className;
            var playerchosen=null;
            if ((/goku/i).test(temp)) {
                playerchosen='goku';
            }else playerchosen="vegeta";
            
            trace(playerchosen);
            
        });
        
    };
    
    this.onRoomJoined=function(){
        $("#feedCredentials").hide();
        $("#choosePlayer").show();
    };
    
}