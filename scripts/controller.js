var DBZ_controller= new function(){
    
    this.gameLoop    = function () {
        var gokuPos=parseInt($("#goku").position().left);
        var vegetaPos=parseInt($("#vegeta").position().left);
        //console.log(gokuPos+" "+vegetaPos);
        if (gokuPos<vegetaPos) {
            $("#vegeta").addClass("faceChange");
            $("#goku").removeClass("faceChange");
        }
        else{
            $("#goku").addClass("faceChange");
            $("#vegeta").removeClass("faceChange");
        }
        
        
        requestAnimationFrame(DBZ_controller.gameLoop);
    };
    
    
    this.bindEvents=function(){
        $(window).keydown(function(e){
            switch (e.keyCode) {
            case 37:
                console.log("left");
                break;
            
            case 38:
                console.log("top");
                break;
            
            case 39:
                console.log("right");
                break;
            }
        });
        
        DBZ_view.bindevents();
    };
    
    this.joinRoomRequest =function(){DBZ_model.joinRoomRequest();};
    
    this.loginRequest=function(loginName){DBZ_model.loginRequest(loginName)};
    
    
    
    
};