var sfsBindEvents=new function(){
    var sfss = null;
    
    this.setSFSobj = function(param){
        sfss= param;
        //console.log(sfss);
        //sfss.connect();
        console.log("is connected??? "+sfss.isConnected());
    };
    
    this.bindEvents  = function(){
        
        sfss.addEventListener(SFS2X.SFSEvent.CONNECTION, onConnect);
        sfss.addEventListener(SFS2X.SFSEvent.CONNECTION_LOST, onConnectionLost);
        sfss.addEventListener(SFS2X.SFSEvent.ROOM_JOIN, onRoomJoineddd);
        sfss.addEventListener(SFS2X.SFSEvent.ROOM_JOIN_ERROR, onRoomJoineddd);
        
        sfss.addEventListener(SFS2X.SFSEvent.LOGIN, onLogin, this);
        sfss.addEventListener(SFS2X.SFSEvent.LOGIN_ERROR, onLoginError, this);
        
        sfss.addEventListener(SFS2X.SFSEvent.USER_COUNT_CHANGE, onUserCountChange, this);
        sfss.addEventListener(SFS2X.SFSEvent.USER_ENTER_ROOM, onUserEnterRoom, this);
        /*sfss.addEventListener(SFS2X.SFSEvent.PUBLIC_MESSAGE, onPublicMessage, this);
        sfss.addEventListener(SFS2X.SFSEvent.ROOM_VARIABLES_UPDATE, onRoomVarsUpdate, this);
        sfss.addEventListener(SFS2X.SFSEvent.LOGOUT, utils.onLogout, this);
        sfss.addEventListener(SFS2X.SFSEvent.EXTENSION_RESPONSE, utils.onExtensionResponse, this);*/ 
        
    }
    
    function onConnect(args) {
        console.log("connected to server? "+ args.success);
        
    }
    
    function onConnectionLost(args) {
        console.log("Disconnected");
        $.each(args,function(key,value){
           console.log(key+" --- "+value);
        });
    }
    
    function onRoomJoineddd(args) {
        trace("room joined!");
        $.each(args,function(key,value){
            trace(key+" --- "+value);
        });
        DBZ_model.setRoomData(args.room);
	DBZ_model.onRoomJoined();
        DBZ_view.onRoomJoined();
    }
    
    function onLogin(args)
    {
        trace("Login successful!");
        $.each(args,function(key,value){
            trace(key+" --- "+value);
        });
        DBZ_model.setLoginData(args.user);
        DBZ_controller.joinRoomRequest();
    }
 
    function onLoginError(evtParams)
    {
        trace("Login failure: " + evtParams.errorMessage);
    }
    
    function onUserCountChange(evtParams)
    {
        var room = evtParams.room;
        var uCount = evtParams.uCount;
        var sCount = evtParams.sCount;
        
        trace("user count update! Room: " + room.name + " now contains " + uCount + " users and " + sCount + " spectators");
    }   
    
    function onUserEnterRoom(evtParams)
    {
	console.log("user enter room");
        var room = evtParams.room;
        var user = evtParams.user;
        
        trace("user enter room! User " + user.name + " just joined Room " + room.name);
    }
    
};